<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Mahasiswa;
use App\Dosen;
use Carbon\Carbon;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->email_verified_at = Carbon::now();
        $user->password = bcrypt('admin');
        $user->user_role = 'admin';
        $user->save();

        $user = new User;
        $user->name = 'mahasiswa';
        $user->email = 'mahasiswa@mahasiswa.com';
        $user->email_verified_at = Carbon::now();
        $user->password = bcrypt('mahasiswa');
        $user->user_role = 'mahasiswa';
        $user->save();

        $mahasiswa = new Mahasiswa;
        $mahasiswa->nrp = '051115400002';
        $mahasiswa->nama = 'Mahasiswa Dummy';
        $mahasiswa->alamat = 'Keputih';
        $mahasiswa->user_id = $user->id;
        
        $user = new User;
        $user->name = 'dosen';
        $user->email = 'dosen@dosen.com';
        $user->email_verified_at = Carbon::now();
        $user->password = bcrypt('dosen');
        $user->user_role = 'dosen';
        $user->save();

        $dosen = new Dosen;
        $dosen->nip = '123123';
        $dosen->namadosen = 'Nama Dosen';
        $dosen->user_id = $user->id;
        $dosen->save();

        $mahasiswa->nip = $dosen->nip;
        $mahasiswa->save();
    }
}
