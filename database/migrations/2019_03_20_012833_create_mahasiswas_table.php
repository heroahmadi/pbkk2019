<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {
            $table->string('nrp', 15);
            $table->primary('nrp');
            $table->string('nama');
            $table->string('alamat');
            $table->string('nip', 10);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('mahasiswas', function(Blueprint $table) {
            $table->foreign('nip')->references('nip')
                                  ->on('dosens')
                                  ->onUpdate('cascade')
                                  ->onDelete('cascade');

            $table->foreign('user_id')->references('id')
                                  ->on('users')
                                  ->onUpdate('cascade')
                                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
