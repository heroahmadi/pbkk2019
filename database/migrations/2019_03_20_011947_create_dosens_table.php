<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosens', function (Blueprint $table) {
            $table->string('nip', 10);
            $table->primary('nip');
            $table->string('namadosen');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('dosens', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')
                                  ->on('users')
                                  ->onUpdate('cascade')
                                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosens');
    }
}
