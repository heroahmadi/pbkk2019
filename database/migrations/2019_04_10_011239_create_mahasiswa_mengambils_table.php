<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaMengambilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_mengambil', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mahasiswa_nrp', 15);
            $table->integer('mata_kuliah_id')->unsigned();
            $table->char('nilai', 1)->nullable();
            $table->timestamps();
        });

        Schema::table('mahasiswa_mengambil', function(Blueprint $table) {
            $table->foreign('mahasiswa_nrp')->references('nrp')
                                  ->on('mahasiswas')
                                  ->onUpdate('cascade')
                                  ->onDelete('cascade');

            $table->foreign('mata_kuliah_id')->references('id')
                                  ->on('mata_kuliahs')
                                  ->onUpdate('cascade')
                                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_mengambil');
    }
}
