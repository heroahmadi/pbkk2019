<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenMengajarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosen_mengajar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dosen_nip', 10);            
            $table->integer('mata_kuliah_id')->unsigned();
            $table->timestamps();
        });


        Schema::table('dosen_mengajar', function(Blueprint $table) {
            $table->foreign('dosen_nip')->references('nip')
                                  ->on('dosens')
                                  ->onUpdate('cascade')
                                  ->onDelete('cascade');
            $table->foreign('mata_kuliah_id')->references('id')
                                  ->on('mata_kuliahs')
                                  ->onUpdate('cascade')
                                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosen_mengajar');
    }
}
