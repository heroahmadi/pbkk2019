<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    public $incrementing = false;
    public $primaryKey = 'nip';

    protected $fillable = ['nip', 'namadosen', 'user_id'];

    public function mahasiswa()
    {
        return $this->hasMany('App\Mahasiswa', 'nip');
    }

    public function mata_kuliah()
    {
        return $this->belongsToMany('App\MataKuliah', 'dosen_mengajar');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
