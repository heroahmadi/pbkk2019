<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    public $incrementing = false;
    public $primaryKey = 'nrp';
    
    protected $fillable = ['nrp', 'nama', 'alamat', 'nip', 'user_id'];

    public function dosen()
    {
        return $this->belongsTo('App\Dosen', 'nip');
    }

    public function frs()
    {
        return $this->hasMany('App\MahasiswaMengambil');
    }

    public function mata_kuliah()
    {
        return $this->belongsToMany('App\MataKuliah', 'mahasiswa_mengambil');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
