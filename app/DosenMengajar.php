<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DosenMengajar extends Model
{
    protected $fillable = ['nip', 'mata_kuliah_id'];
}
