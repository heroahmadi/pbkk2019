<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    protected $fillable = ['id', 'mata_kuliah'];

    public function mahasiswa_mengambil()
    {
        return $this->hasMany('App\MahasiswaMengambil');
    }

    public function dosen()
    {
        return $this->belongsToMany('App\Dosen', 'dosen_mengajar');
    }
}
