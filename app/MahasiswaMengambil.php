<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MahasiswaMengambil extends Model
{
    protected $table = 'mahasiswa_mengambil';

    public function mata_kuliah()
    {
        return $this->belongsTo('App\MataKuliah');
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\Mahasiswa');
    }
}
