<?php

namespace App\Http\Middleware;

use Closure;

class CheckDosen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if($user->user_role == 'dosen')
            return $next($request);
        else
            return back();
    }
}
