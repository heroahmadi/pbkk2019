<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MahasiswaMengambil;
use App\MataKuliah;
use App\Mahasiswa;

class FRSController extends Controller
{
    public function index()
    {
        $data['mahasiswa'] = auth()->user()->mahasiswa;
        // dd($data['mahasiswa']->frs);
        $data['mata_kuliah'] = MataKuliah::all();

        return view('frs.index', $data);
    }

    public function ambil(Request $request)
    {
        $mahasiswa = Mahasiswa::where('user_id', auth()->user()->id)->firstOrFail();
        $mahasiswa->mata_kuliah()->syncWithoutDetaching($request->input('mata_kuliah'));

        return redirect('/frs');
    }

    public function drop(Request $request)
    {
        $frs = MahasiswaMengambil::findOrFail($request->input('id'));
        $frs->delete();

        return redirect('/frs');
    }
}
