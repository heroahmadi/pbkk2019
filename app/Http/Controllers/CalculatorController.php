<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function index(Request $request)
    {
        if($request->input('calc'))
        {
            $persamaan = $request->input('calc');
            $result = eval('return '.$persamaan.';');
            return view('calculator', compact('result', 'persamaan'));
        }
        else
            return view('calculator');
    }
}
