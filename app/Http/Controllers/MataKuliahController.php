<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MataKuliah;
use App\Dosen;
use App\DosenMengajar;
use DB;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['mata_kuliah'] = MataKuliah::paginate(10);

        return view('mata_kuliah.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['dosen'] = Dosen::all();

        return view('mata_kuliah.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = [
                'mata_kuliah' => $request->input('mata_kuliah')
            ];
    
            $matkul = MataKuliah::create($data);
    
            $pengajar = $request->input('dosen_pengajar');
    
            $matkul->dosen()->sync($pengajar);

            DB::commit();
            return redirect('/mata_kuliah');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['mata_kuliah'] = MataKuliah::findOrFail($id);
        $data['dosen'] = Dosen::all();

        return view('mata_kuliah.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mata_kuliah = MataKuliah::findOrFail($id);
        
        DB::beginTransaction();

        try {
            $data = [
                'mata_kuliah' => $request->input('mata_kuliah')
            ];
    
            $mata_kuliah->update($data);
    
            $pengajar = $request->input('dosen_pengajar');
    
            $mata_kuliah->dosen()->sync($pengajar);

            DB::commit();
            return redirect('/mata_kuliah');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mata_kuliah = MataKuliah::findOrFail($id);
        $mata_kuliah->delete();

        return redirect('/mata_kuliah');
    }
}
