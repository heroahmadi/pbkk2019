<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;
use App\Dosen;
use App\User;
use DB;
use Carbon\Carbon;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['mahasiswa'] = DB::table('mahasiswas')->join('dosens', 'mahasiswas.nip', '=', 'dosens.nip')->get();
        $data['mahasiswa'] = Mahasiswa::paginate(10);
        // dd($data);

        return view('mahasiswa.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['dosen'] = Dosen::all();
        return view('mahasiswa.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed'
        ]);

        DB::beginTransaction();

        try {
            $data_user = [
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'name' => $request->input('nama'),
                'email_verified_at' => Carbon::now(),
                'user_role' => 'mahasiswa'
            ];

            $user = User::create($data_user);

            $data = [
                'nrp' => $request->input('nrp'),
                'nama' => $request->input('nama'),
                'alamat' => $request->input('alamat'),
                'nip' => $request->input('nip'),
                'user_id' => $user->id,
            ];
    
            Mahasiswa::create($data);

            DB::commit();
            return redirect('/mhs');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['mahasiswa'] = Mahasiswa::findOrFail($id);
        $data['dosen'] = Dosen::all();
        return view('mahasiswa.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);

        if($request->input('password'))
        {
            $request->validate([
                'password' => 'required|confirmed'
            ]);
        }

        DB::beginTransaction();

        try {
            $data = [
                'nrp' => $request->input('nrp'),
                'nama' => $request->input('nama'),
                'alamat' => $request->input('alamat'),
                'nip' => $request->input('nip')
            ];
    
            $mahasiswa->update($data);

            $data_user = [
                'email' => $request->input('email'),
                'name' => $request->input('nama')
            ];

            if($request->input('password'))
            {
                $data_user['password'] = bcrypt($request->input('password'));
            }

            $mahasiswa->user->update($data_user);

            DB::commit();
            return redirect('/mhs');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return redirect('/mhs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);
        $mahasiswa->delete();

        return redirect('/mhs');
    }
}
