<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\User;
use DB;
use Carbon\Carbon;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['dosen'] = Dosen::all();

        return view('dosen.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed'
        ]);

        DB::beginTransaction();

        try {
            $data_user = [
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'name' => $request->input('nama'),
                'email_verified_at' => Carbon::now(),
                'user_role' => 'dosen'
            ];

            $user = User::create($data_user);
            
            $data_dosen = [
                'nip' => $request->input('nip'),
                'namadosen' => $request->input('nama'),
                'user_id' => $user->id
            ];
    
            Dosen::create($data_dosen);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return redirect('/dosen');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['dosen'] = Dosen::findOrFail($id);
        return view('dosen.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dosen = Dosen::findOrFail($id);

        if($request->input('password'))
        {
            $request->validate([
                'password' => 'required|confirmed'
            ]);
        }

        DB::beginTransaction();

        try {
            $data_dosen = [
                'nip' => $request->input('nip'),
                'namadosen' => $request->input('nama')
            ];
    
            $dosen->update($data_dosen);

            $data_user = [
                'email' => $request->input('email'),
                'name' => $request->input('nama')
            ];

            if($request->input('password'))
            {
                $data_user['password'] = bcrypt($request->input('password'));
            }

            $dosen->user->update($data_user);

            DB::commit();
            return redirect('/dosen');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return redirect('/dosen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dosen = Dosen::findOrFail($id);
        $dosen->delete();

        return redirect('/dosen');
    }
}
