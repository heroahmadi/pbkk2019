<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MahasiswaMengambil;
use DB;

class NilaiController extends Controller
{
    public function index()
    {
        return view('nilai.index');
    }

    public function input($id)
    {
        $data['mengambil'] = MahasiswaMengambil::where('mata_kuliah_id', $id)->get();
        $data['id'] = $id;
        return view('nilai.input', $data);
    }

    public function update(Request $request)
    {
        DB::beginTransaction();

        try {
            foreach ($request->mengambil_id as $key => $value) 
            {
                $mengambil = MahasiswaMengambil::findOrFail($value);
                $mengambil->nilai = $request->input('nilai')[$key];
                $mengambil->save();
            }

            DB::commit();
            return redirect('/nilai/'.$request->input('mata_kuliah_id').'/edit');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
