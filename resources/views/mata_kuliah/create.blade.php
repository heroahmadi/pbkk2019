@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">Create Mata Kuliah</h1>
            </div>
            {{-- <div class="col-md-6 text-right">
                <a href="{{ url('mhs/create') }}" class="btn btn-primary">Add Mahasiswa</a>
            </div> --}}
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <form action="{{ url('mata_kuliah') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="">Mata Kuliah</label>
                        <input type="text" name="mata_kuliah" id="mata_kuliah" placeholder="Mata Kuliah" class="form-group">
                    </div>
                    <div id="pengajar">
                        <div class="form-group">
                            <label for="">Dosen Pengajar</label>
                            <select name="dosen_pengajar[]" id="">
                                <option value="" disabled selected>-- Pilih Dosen Pengajar --</option>
                                @foreach ($dosen as $dsn)
                                    <option value="{{ $dsn->nip }}">{{ $dsn->namadosen }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-success" id="more_dosen">+ Add More Dosen Pengajar</button>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
    <script>
        $("#more_dosen").click(function(){
            $("#pengajar").append(`<div class="form-group">
                <label for="">Dosen Pengajar</label>
                <select name="dosen_pengajar[]" id="">
                    <option value="" disabled selected>-- Pilih Dosen Pengajar --</option>
                    @foreach ($dosen as $dsn)
                        <option value="{{ $dsn->nip }}">{{ $dsn->namadosen }}</option>
                    @endforeach
                </select>
                <button type="button" class="btn btn-danger" onclick=removeDosen(this)>X</button>
            </div>`);
        });

        var removeDosen = function(element){
            $(element).parent('div').remove();
        }
    </script>
@endsection