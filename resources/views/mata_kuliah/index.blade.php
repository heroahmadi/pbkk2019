@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">Mata Kuliah</h1>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ url('mata_kuliah/create') }}" class="btn btn-primary">Add Mata Kuliah</a>
            </div>
        </div>
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Mata Kuliah</th>
                    <th>Dosen Pengajar</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($mata_kuliah as $matkul)
                    <tr>
                        <td>{{ $matkul->mata_kuliah }}</td>
                        <td>
                            @foreach ($matkul->dosen as $pengajar)
                                {{ $pengajar->namadosen }}{{ $loop->last ? '' : ', ' }}
                            @endforeach
                        </td>
                        <td>
                            <form method="POST" action="{{ url("/mata_kuliah/$matkul->id") }}">
                                @csrf
                                @method('DELETE')
                                
                                <a href="{{ url("/mata_kuliah/$matkul->id/edit") }}" class="btn btn-primary">Edit</a>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="text-right">
            {{ $mata_kuliah->links() }}
        </div>
    </div>
@endsection