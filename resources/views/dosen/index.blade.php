@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">Dosen</h1>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ url('dosen/create') }}" class="btn btn-primary">Add Dosen</a>
            </div>
        </div>
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dosen as $dsn)
                    <tr>
                        <td>{{ $dsn->nip }}</td>
                        <td>{{ $dsn->namadosen }}</td>
                        <td>{{ $dsn->user->email }}</td>
                        <td>
                            <form method="POST" action="{{ url("/dosen/$dsn->nip") }}">
                                @csrf
                                @method('DELETE')
                                
                                <a href="{{ url("/dosen/$dsn->nip/edit") }}" class="btn btn-primary">Edit</a>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection