@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">Edit Dosen</h1>
            </div>
            {{-- <div class="col-md-6 text-right">
                <a href="{{ url('mhs/create') }}" class="btn btn-primary">Add Mahasiswa</a>
            </div> --}}
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <form action="{{ url("dosen/$dosen->nip") }}" method="POST">
                    @csrf
                    @method('PUT')
                    
                    <div class="form-group">
                        <label for="">NIP</label>
                        <input type="text" name="nip" id="nip" placeholder="NIP" value="{{ $dosen->nip }}" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" id="nama" placeholder="Nama Dosen" value="{{ $dosen->namadosen }}" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" name="email" id="email" placeholder="Email Dosen" value="{{ $dosen->user->email }}" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" id="password" placeholder="Kosongi apabila tidak ingin mengedit" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Konfirmasi Password</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Kosongi apabila tidak ingin mengedit" class="form-group">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection