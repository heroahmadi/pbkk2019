<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
  <div class="sidebar-heading">PBKK </div>
  <div class="list-group list-group-flush">
    @if (auth()->user()->user_role == 'admin')
    <a href="/dosen" class="list-group-item list-group-item-action bg-light">Dosen</a>
    <a href="/mhs" class="list-group-item list-group-item-action bg-light">Mahasiswa</a>
    <a href="/mata_kuliah" class="list-group-item list-group-item-action bg-light">Mata Kuliah</a>
    @endif

    @if (auth()->user()->user_role == 'dosen')
    <a href="/nilai" class="list-group-item list-group-item-action bg-light">Nilai</a>
    @endif

    @if (auth()->user()->user_role == 'mahasiswa')
    <a href="/frs" class="list-group-item list-group-item-action bg-light">FRS</a>
    @endif
  </div>
</div>
<!-- /#sidebar-wrapper -->