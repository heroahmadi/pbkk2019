@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">Pilih Mata Kuliah</h1>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ url('mhs/create') }}" class="btn btn-primary">Add Pilih Mata Kuliah</a>
            </div>
        </div>
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Mata Kuliah</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach (auth()->user()->dosen->mata_kuliah as $matkul)
                    <tr>
                        <td>{{ $matkul->mata_kuliah }}</td>
                        <td><a href="{{ url("/nilai/$matkul->id/edit") }}" class="btn btn-primary">Input Nilai</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection