@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">Input Nilai</h1>
            </div>
            {{-- <div class="col-md-6 text-right">
                <a href="{{ url('mhs/create') }}" class="btn btn-primary">Input Nilai</a>
            </div> --}}
        </div>
        
        <form action="{{ url('nilai/update') }}" method="POST" autocomplete="off">
            @csrf
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>NRP</th>
                        <th>Nama</th>
                        <th>Nilai</th>
                    </tr>
                </thead>
                <tbody>
                    <input type="hidden" name="mata_kuliah_id" value="{{ $id }}">
                    @foreach ($mengambil as $frs)
                        <input type="hidden" name="mengambil_id[]" value="{{ $frs->id }}">
                        <tr>
                            <td>{{ $frs->mahasiswa->nrp }}</td>
                            <td>{{ $frs->mahasiswa->nama }}</td>
                            <td>
                                <input type="text" name="nilai[]" placeholder="Nilai" value="{{ $frs->nilai ? $frs->nilai : '' }}">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
        </form>
    </div>
@endsection