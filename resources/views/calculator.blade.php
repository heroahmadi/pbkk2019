<html>
    <head>
        <title>Calculator</title>
    </head>
    <body>
        <form action="" method="POST" autocomplete="off">
            @csrf
            <label>Calculator</label>
            <input type="text" name="calc" id="">
        </form>
        @if (isset($result))
            <h3>{{ $persamaan }}</h3>
            <h3>Hasil: {{ $result }}</h3>
        @endif
    </body>
</html>