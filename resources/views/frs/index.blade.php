@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">FRS</h1>
            </div>
        </div>
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Mata Kuliah</th>
                    <th>Dosen Pengajar</th>
                    <th>Nilai</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($mahasiswa->frs as $frs)
                    <tr>
                        <td>{{ $frs->mata_kuliah->mata_kuliah }}</td>
                        <td>
                            @foreach ($frs->mata_kuliah->dosen as $pengajar)
                                {{ $pengajar->namadosen }}{{ $loop->last ? '' : ', ' }}
                            @endforeach
                        </td>
                        <td>{{ $frs->nilai ? $frs->nilai : '-' }}</td>
                        <td>
                            <form method="POST" action="{{ url("/frs/drop") }}">
                                @csrf
                                <input type="hidden" name="id" value="{{ $frs->id }}">
                                
                                <button type="submit" class="btn btn-danger">DROP</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="row">
            <form action="{{ url('frs/ambil') }}" method="POST">
                @csrf
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <div class="form-group">
                        <select name="mata_kuliah" id="" required>
                            <option value="" selected disabled>-- Pilih Mata Kuliah --</option>
                            @foreach ($mata_kuliah as $matkul)
                                <option value="{{ $matkul->id }}">{{ $matkul->mata_kuliah }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">AMBIL</button>
                </div>
                
            </form>
        </div>
    </div>
@endsection