@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">Create Mahasiswa</h1>
            </div>
            {{-- <div class="col-md-6 text-right">
                <a href="{{ url('mhs/create') }}" class="btn btn-primary">Add Mahasiswa</a>
            </div> --}}
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <form action="{{ url('mhs') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="">NRP</label>
                        <input type="text" name="nrp" id="nrp" placeholder="NRP" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" id="nama" placeholder="Nama Mahasiswa" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <input type="text" name="alamat" id="alamat" placeholder="Alamat Mahasiswa" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" name="email" id="email" placeholder="Email Mahasiswa" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" id="password" placeholder="Password Mahasiswa" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Konfirmasi Password</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password Mahasiswa" class="form-group">
                    </div>
                    <div class="form-group">
                        <label for="">Dosen Wali</label>
                        <select name="nip" id="">
                            <option value="" disabled selected>-- Pilih Dosen Wali --</option>
                            @foreach ($dosen as $dsn)
                                <option value="{{ $dsn->nip }}">{{ $dsn->namadosen }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection