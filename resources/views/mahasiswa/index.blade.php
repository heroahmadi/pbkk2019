@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">Mahasiswa</h1>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ url('mhs/create') }}" class="btn btn-primary">Add Mahasiswa</a>
            </div>
        </div>
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>NRP</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Email</th>
                    <th>Dosen Wali</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($mahasiswa as $mhs)
                    <tr>
                        <td>{{ $mhs->nrp }}</td>
                        <td>{{ $mhs->nama }}</td>
                        <td>{{ $mhs->alamat }}</td>
                        <td>{{ $mhs->user->email }}</td>
                        <td>{{ $mhs->namadosen ? $mhs->namadosen : $mhs->dosen->namadosen }}</td>
                        <td>
                            <form method="POST" action="{{ url("/mhs/$mhs->nrp") }}">
                                @csrf
                                @method('DELETE')
                                
                                <a href="{{ url("/mhs/$mhs->nrp/edit") }}" class="btn btn-primary">Edit</a>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection