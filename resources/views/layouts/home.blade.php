<!DOCTYPE html>
<html lang="en">

<head>
  @include('includes.header')
</head>

<body>

  <div class="d-flex" id="wrapper">

    @include('includes.sidebar')

    <!-- Page Content -->
    <div id="page-content-wrapper">

      @include('includes.navbar')

      @yield('content')
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  @include('includes.scripts')

</body>

</html>
