<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::get('/', 'HomeController@index')->name('home');
});

Route::middleware(['admin'])->group(function(){
    Route::resource('/mhs', 'MahasiswaController');
    Route::resource('/dosen', 'DosenController');
    Route::resource('/mata_kuliah', 'MataKuliahController');
});

Route::middleware(['mahasiswa'])->group(function(){
    Route::get('/frs', 'FRSController@index');
    Route::post('/frs/ambil', 'FRSController@ambil');
    Route::post('/frs/drop', 'FRSController@drop');
});

Route::middleware(['dosen'])->group(function(){
    Route::get('/nilai', 'NilaiController@index');
    Route::get('/nilai/{id}/edit', 'NilaiController@input');
    Route::post('/nilai/update', 'NilaiController@update');
});
